#!/bin/bash
# 2018 Lucas
# Ver 1.5.2 - 29-07-2018
# regio var toevoegen
# TLSv1 toevoegen
# terminate script activation
# ------------------------------------------------------------------

# Setting variables
version1=1.5.2
tlsStatus=0
tlsversion=0
verified=0
tls=-
oordeel=0
curTime=0

sleeptimer=$(( $RANDOM % 10 + 60 ))

#echo $sleeptimer
sleep $sleeptimer

#echo Go
# r=$(( $RANDOM % 1000 + 5000 ))

# Get instance ID
instanceID="$(curl --silent --show-error http://169.254.169.254/latest/meta-data/instance-id)"
aws ec2 create-tags --resources $instanceID  --tags Key=AppVer,Value=$version1
aws ec2 create-tags --resources $instanceID  --tags Key=Oordeel,Value=4

# Get MX host info from AWS DynamoDB table
json="$(aws dynamodb scan --table smtptest --filter-expression "oordeel = :oordeel" --expression-attribute-values '{ ":oordeel": { "N": "4" } }' --max 1 | jq -r .Items[])"

#rcount=$((r+100))
#while [  $r -lt $rcount ]; do
#  json="$(aws dynamodb query --table-name smtptest --key-condition-expression "nummer = :nr" --filter-expression "#o = :oordeel" --expression-attribute-names '{"#o": "oordeel"}' --expression-attribute-values  '{":nr":{"N":"'"$r"'"}, ":oordeel":{"N":"0"}}' --max 1 | jq -r .Items[])"
 # echo The counter is $r
#  oordeel="$(echo $json | jq -r .oordeel[])"
 # echo $oordeel
 # echo $json
#  if [[ $oordeel = 0 ]]; then
  #  echo break
#    break
#  fi
#  let r=r+1
#  sleep 1
#done

#json="$(aws dynamodb query --table-name smtptest-t --key-condition-expression "nummer = :nr" --filter-expression "#o = :oordeel" --expression-attribute-names '{"#o": "oordeel"}' --expression-attribute-values  '{":nr":{"N":"4000"}, ":oordeel":{"N":"0"}}') --max 1 | jq -r .Items[]"

# Filling the different variables with with data from the initial query 
mxhostname="$(echo $json | jq -r .mxhostname[])"
domainname="$(echo $json | jq -r .domainname[])"
oordeel="$(echo $json | jq -r .oordeel[])"
nummer="$(echo $json | jq -r .nummer[])"

# Test time stamp get ***** Remove at cleanup
curTime="$(echo `date "+%s"`)"
#echo $curTime

# This update is used to claim the queried mxhost. With TTL. 
oordeel=$((oordeel-1))
aws dynamodb update-item --table-name smtptest --key '{"nummer": {"N": "'"$nummer"'"}}' --update-expression "SET #O = :o, #oordeelTTL= :ot"\
  --expression-attribute-names '{ "#O":"oordeel", "#oordeelTTL":"oordeelttl"}'\
  --expression-attribute-values '{":o":{"N": "'"$oordeel"'"},    ":ot":{"S": "'"$curTime"'" }}'\
  --return-values ALL_NEW

# Debug ***** Remove at cleanup
# echo $json

# Connect to the MX Host with OpenSSL acting as client and using the StartTLS option
smtpOutput="$(openssl s_client -connect $mxhostname:25 -starttls smtp < /dev/null)"

#Test localhost ***** Remove at cleanup
#smtpOutput="$(openssl s_client -connect localhost.:25 -starttls smtp < /dev/null)"

#Echo raw smtp output ***** Remove at cleanup
#echo Output:
#echo $smtpOutput

# Check the smtp output. TLS Weight used. Each test will have a different overall score impact
# check if there was not a connection. ** TLS Weight 0 (doesnt add something to security)
if [ -z "$smtpOutput" ]; then
    tls="$tls | Not Connected ERROR | "
    tlsStatus=$((tlsStatus+0))
    oordeel=$((oordeel-2))
fi

# check if there was a connection. ** TLS Weight 0 (doesnt add something to security)
if [[ $smtpOutput = *"(00000003)"* ]]; then
  tls="$tls | Connected 003 | "
  tlsStatus=$((tlsStatus+0))
  oordeel=$((oordeel+5))
fi

# check if there was not a connection. ** TLS Weight 0 (doesnt add something to security)
if [[ $smtpOutput = *"connect:errno=2"* ]]; then
  tls="$tls | connect:errno=2 | "
  tlsStatus=$((tlsStatus+0))
  oordeel=$((oordeel-3))
fi

# check if there was presented a certificate, check on BEGIN. ** TLS Weight +1 (1st proof of some security)
if [[ $smtpOutput = *"-----BEGIN CERTIFICATE-----"* ]]; then
  tls="$tls | Certificate presented | "
  tlsStatus=$((tlsStatus+1))
fi

# check which protocol was used. ** TLS Weight +3 (2nd proof of some security, TLS 1.2 and above earn +3 )
if [[ $smtpOutput = *"Protocol  : TLSv1.2"* ]]; then
  tls="$tls | TLSver 1.2 | "
  tlsversion="TLSv1.2"
  tlsStatus=$((tlsStatus+3))
fi

# check which protocol was used. ** TLS Weight +2 (2nd proof of some security, TLS 1.1, well at least some security awareity)
if [[ $smtpOutput = *"Protocol  : TLSv1.1"* ]]; then
  tls="$tls | TLSver 1.1 | "
  tlsversion="TLSv1.1"
  tlsStatus=$((tlsStatus+2))
fi

# check which protocol was used. ** TLS Weight +3 (2nd proof of some security, TLS 1.2 and above earn +3 )
if [[ $smtpOutput = *"Protocol  : TLSv1.3"* ]]; then
  tls="$tls | TLSver 1.3 | "
  tlsversion="TLSv1.3"
  tlsStatus=$((tlsStatus+3))
fi

# check which protocol was used. ** TLS Weight +1 (2nd proof of some security, TLS 1.0 shame MUST be better )
if [[ $smtpOutput = *"Protocol  : TLSv1.0"* ]]; then
  tls="$tls | TLSver 1.0 | "
  tlsversion="TLSv1.0"
  tlsStatus=$((tlsStatus+1))
fi

# check which protocol was used. ** TLS Weight -3 (2nd proof of some security, SSL please postmaster act today and get your stuff togheter. )
if [[ $smtpOutput = *"Protocol  : SSL"* ]]; then
  tls="$tls | SSL | "
  tlsversion="SSL"
  tlsStatus=$((tlsStatus-3))
fi

# check if there was presented a certificate that was part of a chain. ** TLS Weight +10 (3rd proof of security)
if [[ $smtpOutput = *"Certificate chain"* ]]; then
  tls="$tls | Part of certificate chain | "
  tlsStatus=$((tlsStatus+10))
fi

# check if there was an error related to the signing of the chain. OpenSSL output:Verify return code: 18 (self signed certificate). ** TLS Weight -20 (3rd proof of security)
if [[ $smtpOutput = *"18 (self signed certificate)"* ]]; then
  tls="$tls | OpenSSL error 18 (self signed certificate) | "
  tlsStatus=$((tlsStatus-20))
  oordeel=$((oordeel+21))
fi

# check if there was an error related to the signing of the chain. OpenSSL output:Verify return code: 19 (self signed certificate in certificate chain). ** TLS Weight -20 (3rd proof of security)
if [[ $smtpOutput = *"19 (self signed certificate in certificate chain)"* ]]; then
  tls="$tls | OpenSSL error 19 (self signed certificate in certificate chain) | "
  tlsStatus=$((tlsStatus-21))
  oordeel=$((oordeel+27))
fi

# check if there was a set of peer certificates presented a certificate. ** TLS Weight -11 (3rd proof of security)
if [[ $smtpOutput = *"no peer certificate available"* ]]; then
  tls="$tls | No Server Certificates available | "
  tlsStatus=$((tlsStatus-11))
  oordeel=$((oordeel+22))
fi

# check if the certificate has expired ** TLS Weight -25 (3rd proof of security)
if [[ $smtpOutput = *"10 (certificate has expired)"* ]]; then
  tls="$tls | 10 (certificate has expired) | "
  tlsStatus=$((tlsStatus-25))
  oordeel=$((oordeel+20))
fi

# check if 1 (unable to verify the first certificate) ** TLS Weight -25 (3rd proof of security)
if [[ $smtpOutput = *"21 (unable to verify the first certificate)"* ]]; then
  tls="$tls | 21 (unable to verify the first certificate) | "
  tlsStatus=$((tlsStatus-11))
  oordeel=$((oordeel+20))
fi

# check if there is not a TLS or Cipher being used.  ** TLS Weight -15 (3rd proof of security)
if [[ $smtpOutput = *"New, (NONE), Cipher is (NONE)"* ]]; then
  tls="$tls | No TLS and Ciipher suites available | "
  tlsStatus=$((tlsStatus-15))
fi

# check if the OpenSSL output is ended with verify OK. Only a small score because Self signed certs are getting the same code. Used to end the verification proces. $oordeel +9 ** TLS Weight +2 (---)
if [[ $smtpOutput = *"Verify return code: 0 (ok)"* ]]; then
  tls="$tls | Verify OK code 0 | "
  verified="OK"
  tlsStatus=$((tlsStatus+2))
  oordeel=$((oordeel+99))
fi

#Echo raw smtp output ***** Remove at cleanup
#echo "Status="$tlsStatus
#echo "Verified="$verified
#echo "TLS Version="$tlsversion
#echo "TLS Info=$tls"
#echo "nummer:$nummer"

# ***** Remove at cleanup
#    aws dynamodb put-item --table-name smtptest --item '{ "mxhostname": {"S": "'"$6"'"}, "domainname": {"S": "'"$1"'"}, "Status": {"S": "0"}, "mxprio": {"S": "'"$5"'"} }' --return-consumed-capacity TOTAL

# Update AWS DynamoDB Table with fetched SMTP connection info ***** Remove at cleanup
aws dynamodb update-item --table-name smtptest --key '{"nummer": {"N": "'"$nummer"'"}}' --update-expression "SET #O = :o, #TLSver= :tv, #TLSinfo= :ti, #TLSstatus= :ts, #verified= :v, #instanceID= :ii"\
 --expression-attribute-names '{ "#O":"oordeel", "#TLSver":"tlsversion", "#TLSinfo":"tlsinfo", "#TLSstatus":"tlsstatus", "#verified":"verified", "#instanceID":"instanceid"}'\
  --expression-attribute-values '{":o":{"N": "'"$oordeel"'"},    ":tv":{"S": "'"$tlsversion"'"},    ":ti":{"S": "'"$tls"'"},    ":ts":{"S": "'"$tlsStatus"'"},    ":v":{"S": "'"$verified"'"},    ":ii":{"S": "'"$instanceID"'"}}'\
  --return-values ALL_NEW

#aws dynamodb update-item --table-name smtpinstance --key '{"instaceid": {"S": "'"$instanceID"'"}}' --update-expression "SET countused = countused + :cu, #lastused= :lu"\
# --expression-attribute-names '{"#lastused":"lastused"}'\
# --expression-attribute-values '{":cu":{"N": "1"},    ":lu":{"N": "'"$curTime"'" }}'\
# --return-values ALL_NEW

randterminate=$(( $RANDOM % 10 + 15 ))

if [[ $randterminate = 24 ]]; then
    #print IP adres to table of server
    instanceID="$(curl --silent --show-error http://169.254.169.254/latest/meta-data/instance-id)" && initdate="$(echo `date "+%s"`)" && aws dynamodb put-item --table-name smtpInstance --item '{"instanceid": {"S": "'"$instanceID T"'"},  "termdate": {"N": "'"$initdate"'" } }'
    sleep 2
    # Terminate command
    aws ec2 terminate-instances --instance-ids $instanceID
fi