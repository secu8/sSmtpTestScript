#!/bin/bash
# 2018 Lucas
# Ver 1.1.0 - 29-07-2018
# ------------------------------------------------------------------

# Setting variables


# Get instance ID
instanceID="$(curl --silent --show-error http://169.254.169.254/latest/meta-data/instance-id)"

#print IP adres to table of server
instanceID="$(curl --silent --show-error http://169.254.169.254/latest/meta-data/instance-id)" && initdate="$(echo `date "+%s"`)" && aws dynamodb put-item --table-name smtpInstance --item '{"instanceid": {"S": "'"$instanceID T"'"},  "termdate": {"N": "'"$initdate"'" } }'


# Terminate command
aws ec2 terminate-instances --instance-ids $instanceID